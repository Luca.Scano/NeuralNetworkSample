﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof( NetworkManager))]
public class NetworkManagerEditor : Editor
{
    NetworkManager nm;
    private SerializedObject manager;

    public void OnEnable()
    {
        manager = new SerializedObject(target);
    }
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        manager.Update();

        nm = (NetworkManager)target;
        if(GUILayout.Button("Burn Them All", GUILayout.Width(100)))
        {
            nm.BurnThemAll();
            
        }
      

      




    }



}