﻿using System;
using UnityEngine;


public class DNA<T>
{
    public float[] genes { get; private set; }
    public float Fitness = 0;


    public DNA( int size)
    {
      
        genes = new float[size];
     
   
            for (int i = 0; i < genes.Length; i++)
            {
                genes[i] = getRandomFloat();

            }
        
    }


    public float getRandomFloat()
    { return UnityEngine.Random.Range(-1f, 1f); }


}