﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GeneticAlgorithm<T>
{
    public List<DNA<T>> population;
    private List<DNA<T>> _elitePopulation = new List<DNA<T>>();
    public float bestFitness;
  
    
    int _populationSize;

    
    private float _mutationRate;

    private List<DNA<T>> newPopulation;
    private System.Random _random;
    public float fitnessSum;
    private int _genomeLenght;
    private int _elitism;
    
    public int mutation = 0;

    /// <summary>
    /// this constructor creates the entire first generation
    /// </summary>
    /// <param name="populationSize"></param>
    /// <param name="dnaSize"></param>
    /// <param name="random"></param>
    /// <param name="getRandomGene"></param>
    /// <param name="fitnessFunction"></param>
    /// <param name="elitism"></param>
    /// <param name="mutationRate"></param>
    public GeneticAlgorithm(GenomeData genomeData,System.Random random)
    {
        _random = random;
        _elitism = genomeData.elitism;
        _populationSize = genomeData.populationSize;
        _mutationRate = genomeData.mutationRate;
        population = new List<DNA<T>>(genomeData.populationSize);
        newPopulation = new List<DNA<T>>(genomeData.populationSize);
        
        this._genomeLenght = genomeData.genomeLenght;
       

    }

    public List<DNA<T>> StartFirstGenesPopulation() {

        for (int i = 0; i < _populationSize; i++)
        {

            population.Add(new DNA<T>(_genomeLenght));

        }
        return population;
    }

    public void CreateNextGenerationOfGenes()
    {

     newPopulation.Clear();
     newPopulation.TrimExcess();

      
        population = SortPopulationByFitness(population);


        if (population.Count > _populationSize)
        {
            population.RemoveRange(_populationSize - 1, _elitism);
        }
        //Debug.Log("pop num "+ population.Count);
        
        _elitePopulation= SaveElite(_elitism,_elitePopulation);
        BreedFromElite(population, _elitism);
        ConcatEliteWithPopulation();
        ClonePopulation(newPopulation, population);

    }

    /// <summary>
    /// Copies  
    /// </summary>
    List<DNA<T>> SaveElite(int elitism, List<DNA<T>> elitePopulation)
    {
        elitePopulation.Clear();
        elitePopulation.TrimExcess();
        for (int i = 0; i < elitism; i++)
        {
            DNA<T> copy = CopyDNA(population[i]);
            copy.Fitness = population[i].Fitness;
            elitePopulation.Add(copy);
        }
        return elitePopulation;
    }

    void ConcatEliteWithPopulation()
    {
        foreach (DNA<T> dna in _elitePopulation)
        {
            newPopulation.Add(dna);
        }
    }

    List<DNA<T>> SortPopulationByFitness(List<DNA<T>> populationToSort)
    {
        populationToSort.Sort(CompareDNA);

        bestFitness = populationToSort[0].Fitness;
        return populationToSort;
    }

    void BreedFromElite(List<DNA<T>> sortedPopulation, int eliteNumber)
    {
        List<DNA<T>> nextGen = new List<DNA<T>>();
        for (int i = 0; i < sortedPopulation.Count -_elitism; i++)
        {

            {

                int lastSelected = RouletteSelect(sortedPopulation, eliteNumber);
                DNA<T> parent1 = sortedPopulation[lastSelected];
                int secondSelected;
                do { secondSelected = RouletteSelect(sortedPopulation, eliteNumber); }
                while (secondSelected == lastSelected);
                DNA<T> parent2 = sortedPopulation[secondSelected];
                DNA<T> child = Crossover(parent1, parent2);

                MutateDNA(child);

                newPopulation.Add(child);
            }
        }
    }

    /// <summary>
    /// This clone copies values, doesnt use List.Add()
    /// </summary>
    /// <param name="donorPopulation"></param>
    /// <param name="receiverPopulation"></param>
    void ClonePopulation(List<DNA<T>> donorPopulation, List<DNA<T>> receiverPopulation)
    {
       
        for (int n = 0; n < donorPopulation.Count; n++)

            for (int i = 0; i < donorPopulation[n].genes.Length; i++)
            {
                receiverPopulation[n].genes[i] = donorPopulation[n].genes[i];
            }
    }


    DNA<T> CopyDNA(DNA<T> toBeCopied)
    {
        DNA<T> copy = new DNA<T>( toBeCopied.genes.Length);

        for (int i = 0; i < toBeCopied.genes.Length; i++)
        {
            copy.genes[i] = toBeCopied.genes[i];
        }
        return copy;
    }


    public void MutateDNA(DNA<T> dna)
    {
        for (int i = 0; i < dna.genes.Length; i++)
        {
            if (_random.NextDouble() < _mutationRate)
            {

                float rand = ((float)_random.NextDouble() * 2) - 1;

                float newWeight = (dna.genes[i]) + rand * (1 - (dna.Fitness / this.bestFitness));
                dna.genes[i] = newWeight; 
            }
        }
    }


    DNA<T> Crossover(DNA<T> parent1, DNA<T> parent2)
    {
        DNA<T> nextDNA = new DNA<T>( _genomeLenght);

        for (int i = 0; i < _genomeLenght; i++)
        {
             float newWeight = Mathf.Lerp(parent1.genes[i], parent2.genes[i], 1 - ((parent1.Fitness) / (parent1.Fitness + parent2.Fitness)));
             nextDNA.genes[i] = newWeight;
        }
        return nextDNA;

    }

    // Returns the selected index based on the weights(probabilities)
    int RouletteSelect(List<DNA<T>> sortedPopulation, int eliteNumber)
    {
        // calculate the total weight
        fitnessSum = 0;
        for (int i = 0; i < eliteNumber; i++)
        {
            fitnessSum += sortedPopulation[i].Fitness;
        }


        // get a random value
        double value = RandUniformPositive() * fitnessSum;
        // locate the random value based on the weights
        for (int i = 0; i < sortedPopulation.Count; i++)
        {
            value -= sortedPopulation[i].Fitness;
            if (value <= 0)
            {
                // Debug.Log("selected "+i);
                return i;
            }
        }
        // when rounding errors occur, we return the last item's index 
        return eliteNumber - 1;
    }

    // Returns a uniformly distributed double value between 0.0 and 1.0
    double RandUniformPositive()
    {
        // easiest implementation
        return _random.NextDouble();
    }



    //a>b -1   a<b 1  a=b 0
    public int CompareDNA(DNA<T> a, DNA<T> b)
    {
        if (a.Fitness > b.Fitness)
        {
            return -1;
        }
        else if (a.Fitness < b.Fitness)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }




}