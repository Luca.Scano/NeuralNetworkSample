﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Genome Data")]
public class GenomeData : ScriptableObject {

    [Header("Genome data")]
    public int populationSize = 200;
    public float mutationRate = 0.01f;
    public int elitism = 1;
    [HideInInspector]
    public int genomeLenght=1;
}
