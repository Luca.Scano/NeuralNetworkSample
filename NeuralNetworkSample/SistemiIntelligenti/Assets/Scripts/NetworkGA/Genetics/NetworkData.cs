﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Neural Network Data")]

public class NetworkData : ScriptableObject {

    

    [Header("Neural Network data")]
    public float weightStandard = 1;
    public float thresholdStandard = 0.5f;
    public int[] numberOfNeuronsPerLayer;
    public static float weight = 1f;

  


}
