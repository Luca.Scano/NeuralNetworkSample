﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connection  {

    public Neuron inputNeuron = new Neuron();
    public float weight;

    /// <summary>
    /// standard constructor
    /// </summary>
    /// <param name="inputNeuron"></param>
    public Connection( Neuron inputNeuron)
    {
        this.inputNeuron = inputNeuron;
        this.weight = Random.Range(-1,1);
    }

    /// <summary>
    /// weighted constructor of the connection
    /// </summary>
    /// <param name="inputNeuron"></param>
    /// <param name="weight"></param>
    public Connection(Neuron inputNeuron, float weight)
    {
        this.inputNeuron = inputNeuron;
        this.weight = weight;
    }



}
