﻿using System.Collections;
using System.Collections.Generic;


public class NNetwork 
{

    public DNA<float> dna ;
    public float[] genome ;
    public List<Layer> layers = new List<Layer>();
    public List<Neuron> inputs = new List<Neuron>();
    public List<Neuron> outputs = new List<Neuron>();

}
