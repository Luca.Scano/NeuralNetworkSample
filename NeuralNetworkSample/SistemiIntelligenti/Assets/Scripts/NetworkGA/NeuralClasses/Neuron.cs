﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Neuron
{

    public float value;
    public List<Connection> connections = new List<Connection>();
    public List<int> connectionsIndex;

    public Neuron()
    {
        value = 0f;
    }

    public float SquashFunction(float feedForwardValue)
    {
        return (float)Math.Tanh(feedForwardValue);
    }


    public void computeStep()
    {
        float partialValue = 0;
        foreach (Connection connection in connections)
        {

            partialValue += connection.weight * connection.inputNeuron.value;

        }
        value = SquashFunction(partialValue);

    }


}
