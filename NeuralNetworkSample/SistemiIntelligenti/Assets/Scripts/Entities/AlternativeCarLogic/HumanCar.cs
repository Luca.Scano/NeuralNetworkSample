﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanCar : MonoBehaviour {

   
    public float minAngle = 1f;
    public float aceleration = .1f;
    public float minVel = 0f;
    public float maxVel = 10f;
    public float currentVel = 0f;
    public float deceleration = .05f;

    public float inputXInTheCar = 0f;
    public float inputYInTheCar = 0f;
   
   
	
	//Testing script for you to drive the car
	void Update () {
      
        
        if (inputXInTheCar != 0 && currentVel != 0)
        {

            this.transform.Rotate(transform.up, minAngle * Time.deltaTime * inputXInTheCar);
        }

        if (inputYInTheCar != 0)
        {

            currentVel = Mathf.Clamp(currentVel + (aceleration * inputYInTheCar), -maxVel, maxVel);
        }
        else
        {

            currentVel = Mathf.Clamp(currentVel - deceleration, minVel, maxVel);
        }

        this.transform.position += this.transform.forward * currentVel * Time.deltaTime;
      
       
        
            inputXInTheCar = Input.GetAxis("Horizontal");
            inputYInTheCar = Input.GetAxis("Vertical");
        
    
       
    }
}
