﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmarterCar : MonoBehaviour
{

    public NetworkManager nm = new NetworkManager();

    public Transform raycastOrigin;
    public Transform raycastRightEnd;
    public Transform raycastLeftEnd;
    public Transform raycastRightEndx;
    public Transform raycastLeftEndx;
    public float minAngle = 1f;
    public float aceleration = .1f;
    public float minVel = 0f;
    public float maxVel = 10f;
    public float currentVel = 0f;
    public float deceleration = .05f;

    public float inputXInTheCar = 0f;
    public float inputYInTheCar = 0f;
    public float vRayDX = 0;
    public float vRaySX = 0;
    public float vRayDXx = 0;
    public float vRaySXx = 0;
    public float vRayF = 0;
    public float fitness = 0;
    public NNetwork net;
    public bool humanPlayer = false;
    float startTime = 0;
    float distanceCovered = 0;
    public Vector3 startPos = Vector3.zero;
    bool isDead = false;

    Vector3 posNow;
    Vector3 posBefore;

    public void Start()
    {
        currentVel = 0f;
        deceleration = .05f;

        inputXInTheCar = 0f;
        inputYInTheCar = 0f;

        vRayDX = 0;
        vRaySX = 0;
        vRayDXx = 0;
        vRaySXx = 0;
        vRayF = 0;
        fitness = 0;
        startTime = Time.time;
        startPos = this.transform.position;
        posBefore = this.transform.position;
    }

    void Update()
    {

        posNow = this.transform.position;
        float newFitness = fitness + (posNow - posBefore).magnitude / Time.deltaTime;
        posBefore = posNow;

        fitness = newFitness;


        //Debug.Log("time passed "+(Time.time - startTime) + " delta time " + );


        #region apply velocity/break
        if (inputXInTheCar != 0 && currentVel != 0)
        {

            this.transform.Rotate(transform.up, (minAngle * Time.deltaTime * inputXInTheCar));//currentVel);
        }

        if (inputYInTheCar != 0)
        {

            currentVel = Mathf.Clamp(currentVel + (aceleration * inputYInTheCar), -maxVel, maxVel);
        }
        else
        {

            currentVel = Mathf.Clamp(currentVel - deceleration, minVel, maxVel);
        }

        this.transform.position += this.transform.forward * currentVel * Time.deltaTime;
        #endregion

        #region raycasting

        vRayDX = 0f;
        vRaySX = 0f;
        vRayDXx = 0;
        vRaySXx = 0;
        vRayF = 0f;

        RaycastHit hit;

        Debug.DrawRay(raycastOrigin.position, raycastRightEnd.position - raycastOrigin.position, Color.red, .1f);
        if (Physics.Raycast(raycastOrigin.position, raycastRightEnd.position - raycastOrigin.position, out hit, 10f))
        {

            if (hit.collider.gameObject.CompareTag("Track"))
            {
                //Debug.Log("rigth "+hit.distance / 10);
                vRayDX = hit.distance / 10;
            }
        }

        Debug.DrawRay(raycastOrigin.position, raycastRightEndx.position - raycastOrigin.position, Color.red, .1f);
        if (Physics.Raycast(raycastOrigin.position, raycastRightEndx.position - raycastOrigin.position, out hit, 10f))
        {

            if (hit.collider.gameObject.CompareTag("Track"))
            {
                //Debug.Log("rigth "+hit.distance / 10);
                vRayDXx = hit.distance / 10;
            }
        }


        Debug.DrawRay(raycastOrigin.position, raycastOrigin.forward * (raycastLeftEnd.position - raycastOrigin.position).magnitude, Color.green, .1f);
        if (Physics.Raycast(raycastOrigin.position, raycastOrigin.forward * (raycastLeftEnd.position - raycastOrigin.position).magnitude, out hit, 18f))
        {

            if (hit.collider.gameObject.CompareTag("Track"))
            {
                //Debug.Log("front "+hit.distance / 10);
                vRayF = hit.distance / 18;
            }
        }


        Debug.DrawRay(raycastOrigin.position, raycastLeftEnd.position - raycastOrigin.position, Color.red, .1f);
        if (Physics.Raycast(raycastOrigin.position, raycastLeftEnd.position - raycastOrigin.position, out hit, 10f))
        {

            if (hit.collider.gameObject.CompareTag("Track"))
            {
                //Debug.Log("left " + hit.distance/ 10);
                vRaySX = hit.distance / 10;
            }
        }



        Debug.DrawRay(raycastOrigin.position, raycastLeftEndx.position - raycastOrigin.position, Color.red, .1f);
        if (Physics.Raycast(raycastOrigin.position, raycastLeftEndx.position - raycastOrigin.position, out hit, 10f))
        {

            if (hit.collider.gameObject.CompareTag("Track"))
            {
                //Debug.Log("left " + hit.distance/ 10);
                vRaySXx = hit.distance / 10;
            }
        }



        #endregion

        //creo lista
        List<float> raycasts = new List<float>();
        raycasts.Clear();
        raycasts.Add(vRayDX);
        raycasts.Add(vRaySX);
        raycasts.Add(vRayDXx);
        raycasts.Add(vRaySXx);
        raycasts.Add(vRayF);


        //feedforward
        NetworkManager.FeedForward(net, raycasts);

        //assegno output della rete al comando della macchina
        #region input application
        if (humanPlayer)
        {
            inputXInTheCar = Input.GetAxis("Horizontal");
            inputYInTheCar = Input.GetAxis("Vertical");
        }
        else
        {
            //Debug.Log("driving the car "+net.outputs[0].value+ net.outputs[1].value);
            inputXInTheCar = net.outputs[0].value;
            inputYInTheCar = net.outputs[1].value;

        }
        #endregion

        if (currentVel < 0)
        {
            Die();
        }

        if (isDead)
        {
            isDead = false;
            net.dna.Fitness = fitness;

            NetworkManager .entityAlive--;
            this.gameObject.SetActive(false);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Track"))
            Die();

    }

    public void Die()
    {

        isDead = true;
    }


}
