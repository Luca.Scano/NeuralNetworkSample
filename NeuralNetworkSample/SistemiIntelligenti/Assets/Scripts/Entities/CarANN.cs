﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarANN : A_Entity {

    /// <summary>
    /// Public fields are public for convenience, just to see them in the inspector
    /// i know it's horrible, [SerializedField] would be a better choice
    /// but they are so many fields that i cant even.. 
    /// </summary>
    public Transform raycastOrigin;
    public Transform raycastRightEnd;
    public Transform raycastLeftEnd;
    public float minAngle = 1f;
    public float aceleration = .1f;
    public float minVel = 0f;
    public float maxVel = 10f;
    public float currentVel = 0f;
    public float deceleration = .05f;

    public float inputXInTheCar = 0f;
    public float inputYInTheCar = 0f;
    public float vRayDX = 0;
    public float vRaySX = 0;
    public float vRayF = 0;

   
  
    float startTime = 0;
    
    public Vector3 startPos = Vector3.zero;
   

    Vector3 posNow;
    Vector3 posBefore;

     void Start()
    {
        startPos = this.transform.position;
        InitEntity();
    }

    /// <summary>
    /// Called every frame from the manager
    /// </summary>
    /// <returns> Returns the input to feed the network to get the computed output
    /// returns>
    public override List<float> FrameUpdate() {

        
        List<float> InputToBeFedToTheNetwork  = new List<float>();

       if (currentVel < 0)
       {
           Die();
       }


        {
            UpdateFitness();


            #region apply velocity/break
            if (inputXInTheCar != 0 && currentVel != 0)
            {

                this.transform.Rotate(transform.up, (minAngle * Time.deltaTime * inputXInTheCar));//currentVel);
            }

            if (inputYInTheCar != 0)
            {

                currentVel = Mathf.Clamp(currentVel + (aceleration * inputYInTheCar), -maxVel, maxVel);
            }
            else
            {

                currentVel = Mathf.Clamp(currentVel - deceleration, minVel, maxVel);
            }

            this.transform.position += this.transform.forward * currentVel * Time.deltaTime;
            #endregion

            #region raycasting

            vRayDX = 0f;
            vRaySX = 0f;
            vRayF = 0f;

            RaycastHit hit;

            Debug.DrawRay(raycastOrigin.position, raycastRightEnd.position - raycastOrigin.position, Color.red, .1f);
            if (Physics.Raycast(raycastOrigin.position, raycastRightEnd.position - raycastOrigin.position, out hit, 10f))
            {

                if (hit.collider.gameObject.CompareTag("Track"))
                {

                    vRayDX = hit.distance / 18;
                }
            }

            Debug.DrawRay(raycastOrigin.position, raycastOrigin.forward * (raycastLeftEnd.position - raycastOrigin.position).magnitude, Color.green, .1f);
            if (Physics.Raycast(raycastOrigin.position, raycastOrigin.forward * (raycastLeftEnd.position - raycastOrigin.position).magnitude, out hit, 18f))
            {

                if (hit.collider.gameObject.CompareTag("Track"))
                {
                    //Debug.Log("front "+hit.distance / 10);
                    vRayF = hit.distance / 10;
                }
            }


            Debug.DrawRay(raycastOrigin.position, raycastLeftEnd.position - raycastOrigin.position, Color.red, .1f);
            if (Physics.Raycast(raycastOrigin.position, raycastLeftEnd.position - raycastOrigin.position, out hit, 10f))
            {

                if (hit.collider.gameObject.CompareTag("Track"))
                {
                    //Debug.Log("left " + hit.distance/ 10);
                    vRaySX = hit.distance / 18;
                }
            }

            #endregion

            //This list
            List<float> raycasts = new List<float>();
            raycasts.Clear();
            raycasts.TrimExcess();
            raycasts.Add(vRayDX);
            raycasts.Add(vRaySX);
            raycasts.Add(vRayF);

            InputToBeFedToTheNetwork = raycasts;
       
        }
        return InputToBeFedToTheNetwork;

    }

    

    public override void AssignNeuralOutputToEntity() {
        //assegno output della rete al comando della macchina
        #region input application

        inputXInTheCar = net.outputs[0].value;
        inputYInTheCar = net.outputs[1].value;

        #endregion
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Track"))
            Die();

    }

    public override void Die() {

        isDead = true;
        net.dna.Fitness = fitness;

        NetworkManager.entityAlive--;
        this.gameObject.SetActive(false);
    }


    public override void UpdateFitness() {
        posNow = this.transform.position;
        float newFitness = fitness + (posNow - posBefore).magnitude/Time.deltaTime ;
        posBefore = posNow;

        fitness = newFitness;
    }

    public override void  InitEntity()
    {
        currentVel = 0f;
        deceleration = .05f;
        isDead = false;

        inputXInTheCar = 0f;
        inputYInTheCar = 0f;

        vRayDX = 0;
        vRaySX = 0;
        vRayF = 0;
        fitness = 0;
        startTime = Time.time;
        this.transform.position = startPos ;
        posBefore = this.transform.position;
        this.gameObject.SetActive(true);
    } 
  
}
