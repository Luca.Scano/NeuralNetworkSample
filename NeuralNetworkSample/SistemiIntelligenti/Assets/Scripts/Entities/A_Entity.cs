﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class A_Entity : MonoBehaviour {

    public float fitness = 0;
    public NNetwork net;
    public bool isDead = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateFitness();
	}

    public abstract  void AssignNeuralOutputToEntity();
    public abstract List<float> FrameUpdate();
    public abstract void InitEntity();
    public abstract void UpdateFitness();
    public abstract void Die();
}
