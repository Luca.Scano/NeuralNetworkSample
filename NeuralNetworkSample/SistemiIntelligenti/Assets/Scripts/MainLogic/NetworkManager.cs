﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class NetworkManager : MonoBehaviour
{
    /// <summary>
    /// This is the main class of the project
    /// The logic steps in order to create a new generation of Artificial Neural Networks that drive Entities are the following
    /// 
    /// #n is the population size decided by the user
    /// * START LOGIC *
    ///          1)* we instantiate a new genetic algorithm which 
    ///              creates the genetic pool of #n genomes  using the supplied parameters from GenomeData scriptableObject
    ///          
    ///          2)* we create a "pool of brains",in other words #n neural networks ,using the parameters from NeuralNetworkData scriptableObject
    ///          
    ///          3)* we instantiate #n Entities as instances of a supplied prefab
    ///          
    ///          4)* we bind one brain (one neural net) with one entity (in our case a car) and we do that for every individual of the population
    ///          (A_Entity.instaciateEntity() takes care of this)
    /// *FRAME LOGIC*
    /// 
    ///             Every frame this manager checks if any entity is still alive, if so it updates any alive entity.
    ///             Every entity each fram gathers some data in form of List<float> to be fed to its own neural network (its "brain")
    ///             The FeedForward() method takes care of computing output data for each entity,then this ouput is applied till the next frame
    /// </summary>


    [Header("Functions as editor of the network")]

    #region data supplied by the user
    public Transform spawnTransform;
    public NetworkData networkData;
    public GenomeData genomeData;
    public GameObject entityPrefab;
    #endregion
    #region population info in inspector
    public int generation = 0;
    public static int entityAlive = 0;
    public float maxFitness = 0f;
    public float averageFitness = 0f;
    #endregion

    /// <summary>
    /// Here starts all the Logic
    /// </summary>
    void Start()
    {

        LocalFieldsInit();
        /// 1)*
        ga.StartFirstGenesPopulation();



        netFactory = new NetworkFactory(this);

        ///Here is specified that the factory is a CarFactory,
        ///but a inspector field can be added 
        ///in order to choose between different factories maybe with an enumerator
        ///
        carFactory = new CarFactory();

        /// 2)*
        CreateNetworkPopulationFromeGenes(ga.population);
        /// 3)*
        CreateEntityPopulationFromNetworks(networkPopulation);

    }

    /// <summary>
    /// Called if the population is all dead
    /// </summary>
    void NextGeneration()
    {
        //Updates the genes with the best feature through selection and crossover
        ga.CreateNextGenerationOfGenes();
        #region population info in inspector
        maxFitness = ga.bestFitness;
        averageFitness = ga.fitnessSum / _populationSize;
        #endregion


        //goes through the entire entities' population
        for (int currentIndividual = 0; currentIndividual < networkPopulation.Count; currentIndividual++)
        {
            entityAlive++;
            ///Initialization of the new generation with object pooling
            ///that means that the entities are not destroyed and recreated 
            ///but instead just the "brains" are updated with the new genes
            netFactory.UpdateCurrentNetwork(ga.population[currentIndividual].genes,
                entityPopulation[currentIndividual].GetComponent<A_Entity>().net);

            entityPopulation[currentIndividual].GetComponent<A_Entity>().InitEntity();

            

        }

        generation++;
    }

    /// <summary>
    /// Checks if there are any car left alive, if not it loads a new population
    /// </summary>
    void Update()
    {

        
        if (entityAlive <= 0)
        {

            NextGeneration();
        }
        else
        {
            EntitesFrameUpdate();
        }

    }


    /// <summary>
    /// This is the only update there is, the control of the entities is centralized in here
    /// </summary>
    void EntitesFrameUpdate()
    {
        
        foreach (GameObject entity in entityPopulation)
        {
            A_Entity _entity = entity.GetComponent<A_Entity>();


            //feedforward
            if (!_entity.isDead)
            {
                List<float> inputList = _entity.FrameUpdate();
              
                FeedForward(_entity.net,inputList );
                _entity.AssignNeuralOutputToEntity();
            }
        }

    }

    //ENTITIES POPULATION
    /// <summary>
    /// Creates the entire entity GO population from a given genome pool
    /// </summary>
    /// <param name="networkPopulation"></param>
    /// 
    void CreateEntityPopulationFromNetworks(List<NNetwork> networkPopulation)
    {

        foreach (NNetwork net in networkPopulation)
        {
            ///4)*
            GameObject entity = carFactory.InstatiateEntity(net, entityPrefab, spawnTransform);

            entityPopulation.Add(entity);
            entityAlive++;
        }

    }


    //NETWORKS POPULATION
    /// <summary>
    /// Creates a network population given a DNA population
    /// </summary>
    /// <param name="Population"></param>
    void CreateNetworkPopulationFromeGenes(List<DNA<float>> Population)
    {
        //creo la lista con tutte le reti
        NetworkFactory factory = new NetworkFactory(this);
        int i = 1;
        foreach (DNA<float> dna in Population)
        {
            NNetwork net = factory.CreateNewNetworkFromScratch(dna);
            i++;
            networkPopulation.Add(net);
        }

    }




    /// <summary>
    /// Sets the input values array as values of the Input Neurons
    /// </summary>
    /// <param name="network"></param>
    /// <param name="inputValues"></param>
    public static void BindInput(NNetwork network, List<float> inputValues)
    {
        for (int inputIndex = 0; inputIndex < inputValues.Count; inputIndex++)
        {
            network.inputs[inputIndex].value = inputValues[inputIndex];
        }

    }

    /// <summary>
    /// Feed forward of the entire network one layer at the time
    /// </summary>
    /// <param name="netToFeed"></param>
    /// <param name="inputValues"></param>
    public static void FeedForward(NNetwork netToFeed, List<float> inputValues)
    {
        BindInput(netToFeed, inputValues);
        for (int layerIndex = 1; layerIndex < netToFeed.layers.Count; layerIndex++)
        {
            foreach (Neuron neuron in netToFeed.layers[layerIndex].neurons)
            {
                //Debug.Log("forward");
                if (neuron.GetType() != typeof(BiasNeuron))
                    neuron.computeStep();
            }
        }
    }

    private void LocalFieldsInit()
    {
        random = new System.Random();
        _populationSize = genomeData.populationSize;
        _mutationRate = genomeData.mutationRate;
        _elitism = genomeData.elitism;
        _weightStandard = networkData.weightStandard;
        _thresholdStandard = networkData.thresholdStandard;
        _numberOfNeuronsPerLayer = networkData.numberOfNeuronsPerLayer;
        _weight = _weightStandard;

        genomeData.genomeLenght = NetworkFactory.NumberOfConnections(_numberOfNeuronsPerLayer);
        ga = new GeneticAlgorithm<float>(genomeData,random);
    }

    //Kills all the population
    public void BurnThemAll()
    {

        foreach (GameObject entity in entityPopulation)
        {
            if (entity.activeSelf)
                entity.GetComponent<A_Entity>().Die();
        }


    }

    //Runtime Created (logic)
    private A_EntityFactory carFactory;
    private NetworkFactory netFactory;
    private GeneticAlgorithm<float> ga;



    //Runtime Created ()
    private List<NNetwork> networkPopulation = new List<NNetwork>();
    private List<GameObject> entityPopulation = new List<GameObject>();
    private System.Random random;

    //Runtime assigned
    private int _populationSize = 200;
    private float _mutationRate = 0.01f;
    private int _elitism = 1;
    private float _weightStandard = 1;
    private float _thresholdStandard = 0.5f;
    private int[] _numberOfNeuronsPerLayer;
    private static float _weight = 1f;



}
