﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class A_EntityFactory : MonoBehaviour {

    public GameObject _entityPrefab;

   

    public abstract GameObject InstatiateEntity(NNetwork network, GameObject entityPrefab,Transform spawnPoint);
 

}
