﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarFactory : A_EntityFactory {

    //Overrides
    public override GameObject InstatiateEntity(NNetwork network, GameObject entityPrefab,Transform spawnTransform)
    {

        GameObject newEntity = Instantiate(entityPrefab, spawnTransform.position, Quaternion.identity);
        ///
        newEntity.GetComponent<CarANN>().net = network;
        return newEntity;

    }


}
