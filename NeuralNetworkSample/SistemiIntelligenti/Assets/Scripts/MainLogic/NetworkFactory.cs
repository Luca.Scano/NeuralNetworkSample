﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkFactory 
{
    NetworkManager nm;
    int[] numberOfNeuronsPerLayer;
    public NetworkFactory(NetworkManager nm)
    {
        this.nm = nm;
        this.numberOfNeuronsPerLayer = nm.networkData.numberOfNeuronsPerLayer;
       
    }

    /// <summary>
    /// Creates the entire network with fresh neurons
    /// </summary>
    /// <returns></returns>
    public NNetwork CreateNewNetworkFromScratch(DNA<float> dna)
    {
        
        int neurons = 0;
        int bias = 0;
        //fresh network
        NNetwork newNetwork = new NNetwork();
        int numberOfLayers = numberOfNeuronsPerLayer.Length;

        newNetwork.genome = dna.genes;

        #region populate the network with fresh neurons
        for (int currentLayer = 0; currentLayer < numberOfLayers; currentLayer++)
        {
            newNetwork.layers.Add(new Layer());
            //the +1 is due to the fact that i add a bias "hidden" neuron
            for (int currentNeuron = 0; currentNeuron <= numberOfNeuronsPerLayer[currentLayer]; currentNeuron++)
            {

                if (currentNeuron < numberOfNeuronsPerLayer[currentLayer])
                {
                    newNetwork.layers[currentLayer].neurons.Add(new Neuron());
                    neurons++;
                }
                //add bias neuron if not output nodes
                else if (currentLayer + 1 != numberOfLayers)
                {
                    newNetwork.layers[currentLayer].neurons.Add(new BiasNeuron());
                    bias++;
                }

            }
        }
   
        #endregion

        ConnectLayers(newNetwork);
        SetIONeurons(newNetwork);
        newNetwork.dna= dna;
        return newNetwork;
    }


    public void UpdateCurrentNetwork(float[] freshGenome, NNetwork newNetwork)
    {
       
        int numberOfLayers = numberOfNeuronsPerLayer.Length;

        newNetwork.genome = freshGenome;

        UpdateWeights(newNetwork);

    }


    public void UpdateWeights(NNetwork networkToUpdate)
    {
        int connectionNumber = 0;
 
        for (int currentIndexLayer = 0; currentIndexLayer < networkToUpdate.layers.Count - 1; currentIndexLayer++)
        {       
            for (int currentIndexNeuron = 0; currentIndexNeuron < networkToUpdate.layers[currentIndexLayer].neurons.Count; currentIndexNeuron++)
            {
                Neuron currentNeuron = networkToUpdate.layers[currentIndexLayer].neurons[currentIndexNeuron];
               
                foreach (Neuron neuron in networkToUpdate.layers[currentIndexLayer + 1].neurons)
                {
                    if (neuron.GetType() != typeof(BiasNeuron))
                    {
                       // Debug.Log("update");
                        //Debug.Log("was "+ neuron.connections[i].weight + "is "+ networkToUpdate.genome[connectionNumber - 1]);
                        neuron.connections[currentIndexNeuron].weight = networkToUpdate.genome[connectionNumber ];
                        if (neuron.connections[currentIndexNeuron].weight == 0f)
                            Debug.Log(0);
                        connectionNumber++;
                                              
                    }
                    
                }
                
            }
        }
    }


    /// <summary>
    /// Connects all the neurons on each layers of a given network
    /// </summary>
    /// <param name="networkToConnect"></param>
    static void ConnectLayers(NNetwork networkToConnect)
    {
        int connectionNumber = 1;
        //loops Layers
        for (int currentIndexLayer = 0; currentIndexLayer < networkToConnect.layers.Count - 1; currentIndexLayer++)
        {
           
            Layer currentLayer = networkToConnect.layers[currentIndexLayer];
            //loops Neurons in Layers
            for (int currentIndexNeuron = 0; currentIndexNeuron < currentLayer.neurons.Count; currentIndexNeuron++)
            {
                Neuron currentNeuron = currentLayer.neurons[currentIndexNeuron];
             
                foreach (Neuron neuron in networkToConnect.layers[currentIndexLayer + 1].neurons)
                {
                    if (neuron.GetType() != typeof(BiasNeuron))
                    {   
                        Connection connectionHandle;
                        connectionHandle = MakeConnection(currentNeuron, neuron, networkToConnect.genome[connectionNumber - 1]);
                      
                        connectionNumber++;
                    }
                   
                    
                }


            }
        }
    
    }

    /// <summary>
    /// Sets the I/O neurons in the network data set
    /// 
    /// </summary>
    /// <param name="network"></param>
    static void SetIONeurons(NNetwork network)
    {
        
        network.inputs = network.layers[0].neurons;
        network.outputs = network.layers[network.layers.Count - 1].neurons;

    }

    /// <summary>
    /// Adds the connection with the given output
    /// neuron with the connection with the input neuron
    /// with random weights
    /// </summary>
    /// <param name="inputNeuron"></param>
    /// <param name="outputNeuron"></param>
    static Connection MakeConnection(Neuron currentNeuron, Neuron outputNeuron,float weight)
    {
        Connection freshConnection = new Connection(currentNeuron,weight);
        outputNeuron.connections.Add(freshConnection);
        return freshConnection;
    }


    /// <summary>
    /// Calculates the number of total connections in the network and therefore the number of genes required
    /// </summary>
    /// <param name="numberOfNeuronsPerLayer"></param>
    /// <returns></returns>
    public static int NumberOfConnections(int[] numberOfNeuronsPerLayer)
    {
        int totalConnections = 0;
        for (int i = 0; i < numberOfNeuronsPerLayer.Length - 1; i++)
        {
            totalConnections += (numberOfNeuronsPerLayer[i] + 1) * numberOfNeuronsPerLayer[i + 1];
        }
        return totalConnections;
    }

}
